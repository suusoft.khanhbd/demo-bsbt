function googleTranslateElementInit() {
  setCookie("googtrans", "/en/en", 1);
  new google.translate.TranslateElement(
    {
      pageLanguage: "en",
      includedLanguages: "en,zh-CN,ja,ko,ru,vi",
      autoDisplay: false,
      layout: google.translate.TranslateElement.InlineLayout.SIMPLE
    },
    "google_translate_element"
  );
  setDefaultLanguageTextDropdown();
}
function setDefaultLanguageTextDropdown() {
  try {
    var a = document.querySelector("#google_translate_element .goog-te-menu-value span");
    if (a.textContent == "Select Language") {
      a.textContent = "English";
    } else {
      setTimeout(() => {
        setDefaultLanguageTextDropdown();
      }, 100);
    }
  } catch (error) {
    console.log(error);
  }
}
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

window.onload = function() {
  let myiFrame = document.getElementsByClassName("goog-te-menu-frame")[0];
  let doc = myiFrame.contentDocument;
  let style = document.createElement("link");

  style.type = "text/css";
  style.rel = "stylesheet";
  style.href = "style-gg.css";

  doc.head.appendChild(style);

  window.onscroll = function() {
    myiFrame.style.display = "none";
  };
  window.click = function() {
    myiFrame.style.display = "none";
  };
  myiFrame.click = function() {
    myiFrame.style.display = "none";
  };
};
