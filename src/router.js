import Vue from "vue";
import VueRouter from "vue-router";

// Page
import HomePage from "./views/HomePage";
import TopWinner from "./views/ListTopWinner.vue";
import NotFound from "./views/NotFound.vue";
import ErrorPage from "./views/ErrorPage.vue";
import SettingPage from "./views/SettingPage.vue";
import ComingSoon from "./views/ComingSoon.vue";
import TosPage from "./views/TosPage.vue";

// Withdraw
import Withdraw from "./views/Withdraw.vue";
import WithdrawHistory from "./views/WithdrawHistory.vue";

// Create Quiz - Survey
import CreateQuiz from "./views/CreateQuiz";
import CreateSurvey from "./views/CreateSurvey";
import QuizSurveySubmit from "./views/QuizSurveySubmit";

// List Quiz - Survey
import ListQuiz from "./views/ListQuiz";
import ListSurvey from "./views/ListSurvey";

// Play Quiz
import QuizIntro from "./views/QuizIntro";
import QuizAds from "./views/QuizAds";
import QuizPlay from "./views/QuizPlay";
import QuizResult from "./views/QuizResult";

// Play Survey
import SurveyPlay from "./views/SurveyPlay";

// Auth
import SignIn from "./views/SignIn";
import SignUp from "./views/SignUp";
import SignUpSuccess from "./views/SignUpSuccess";
import ScanQrApp from "./views/ScanQrApp";

import { apiCustomerPost } from "./services/baseApi.js";

Vue.use(VueRouter);
const title = 'Bitstoragebox';
const routes = [
  {
    path: "/",
    name: "Home",
    component: HomePage,
    meta: { title: title + " | Home" }
  },
  {
    path: "/create-quiz",
    name: "CreateQuiz",
    component: CreateQuiz,
    meta: { title: title + " | Create Quiz" }
  },
  {
    path: "/create-survey",
    name: "CreateSurvey",
    component: CreateSurvey,
    meta: { title: title + " | Create Survey" }
  },
  {
    path: "/list-quiz",
    name: "ListQuiz",
    component: ListQuiz,
    meta: { title: title + " | List Quiz" }
  },
  {
    path: "/list-survey",
    name: "ListSurvey",
    component: ListSurvey,
    meta: { title: title + " | List Survey" }
  },
  {
    path: "/quiz-ads",
    name: "QuizAds",
    component: QuizAds,
    meta: { title: title + " | Quiz Ads" }
  },
  {
    path: "/quiz-intro",
    name: "QuizIntro",
    component: QuizIntro,
    meta: { title: title + " | Intro Quiz" }
  },
  {
    path: "/quiz-play",
    name: "QuizPlay",
    component: QuizPlay,
    meta: { title: title + " | Play Quiz" }
  },
  {
    path: "/survey-play",
    name: "SurveyPlay",
    component: SurveyPlay,
    meta: { title: title + " | Play Survey" }
  },
  {
    path: "/quiz-result",
    name: "QuizResult",
    component: QuizResult,
    meta: { title: title + " | Quiz Result" }
  },
  {
    path: "/signin",
    name: "SignIn",
    component: SignIn,
    meta: { title: title + " | Sign In" }
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
    meta: { title: title + " | Sign Up" }
  },
  {
    path: "/signup-success",
    name: "SignUpSuccess",
    component: SignUpSuccess,
    meta: { title: title + " | Sign Up" }
  },
  {
    path: "/scan-qr-app/:type",
    name: "ScanQrApp",
    component: ScanQrApp,
    meta: { title: title + " | Scan Qr App" }
  },
  {
    path: "/top-winners",
    name: "TopWinner",
    component: TopWinner,
    meta: { title: title + " | Top Winners" }
  },
  {
    path: "/setting",
    name: "SettingPage",
    component: SettingPage,
    meta: { title: title + " | Setting" }
  },
  {
    path: "/submit-success",
    name: "QuizSurveySubmit",
    component: QuizSurveySubmit,
    meta: { title: title + " | Submit Success" }
  },
  {
    path: "/tos",
    name: "TosPage",
    component: TosPage,
    meta: { title: title + " | Tos" }
  },
  {
    path: "/withdraw",
    name: "Withdraw",
    component: Withdraw,
    meta: { title: title + " | Withdraw" }
  },
  {
    path: "/withdraw-history",
    name: "WithdrawHistory",
    component: WithdrawHistory,
    meta: { title: title + " | Withdraw History" }
  },
  {
    path: "/coming-soon",
    name: "ComingSoon",
    component: ComingSoon,
    meta: { title: title + " | Coming Soon" }
  },
  {
    path: "/error",
    name: "ErrorPage",
    component: ErrorPage,
    meta: { title: title + " | Error" }
  },
  {
    path: "*",
    name: "Not Found",
    component: NotFound,
    meta: { title: title + " | Not Found" }
  }
];

const router = new VueRouter({
  routes,
  mode: "history",
  scrollBehavior(to, from) {
    if (from.path.includes('quiz-intro')) {
      return { y: -1 }
    } else {
      return { x: 0, y: 0 };
    }
  },
});

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("token");
  const check_play = localStorage.getItem('check_play')

  document.title = to.meta.title;

  let path = to && to.path;

  if (from && (from.path === path) && (from.name === to.name)) {
    return false;
  }
  // Validate User
  if (
    (!token) &&
    (path.includes("play") ||
      path.includes("ads") ||
      path.includes("intro") ||
      path.includes("create") ||
      path.includes("result") ||
      path.includes("withdraw") ||
      path.includes("submit"))
  ) {
    next("/signin");
    return;
  }

  if (path.includes('quiz-play') && !from.path.includes('quiz-ads')) {
    next('/')
    return
  }

  if (from.path.includes('quiz-play') && !path.includes('quiz-result') && !check_play) {
    if (confirm(' Your changes have not been saved, would you like to redirect?')) {
      let body = localStorage.getItem('not-submited')
      if (body) {
        body = JSON.parse(localStorage.getItem('not-submited'));
        apiCustomerPost("start_quiz/submit", body);
        localStorage.removeItem('not-submited')
      }
      next('/quiz-result');
    }
  }

  // Coming Soon
  if (path.includes('create-survey')) {
    next("/coming-soon");
    return;
  }

  next();
});

export default router;
