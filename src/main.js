import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store/store.js'
import moment from 'moment'
import VueYoutube from 'vue-youtube'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import authGoogle from './services/authGoogle'
import './registerServiceWorker'

Vue.config.productionTip = false

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(value).format('MM/DD/YYYY')
  }
});
Vue.filter('btcToUsd', function(value) {
  const price = localStorage.getItem('btc-price');

  if (!price || !value) return 0;
  return parseFloat(price*Number(value)).toFixed(4);
});
Vue.filter('bsbtToUsd', function(value) {
  const price = localStorage.getItem('bsbt-price');

  if (!price || !value) return 0;
  return parseFloat(price*Number(value)).toFixed(4);
});

Vue.filter('formatDateMonth', function (value) {
  if (value) {
    return moment(value).format('DD MMMM YYYY')
  }
});

const gauthOption = {
  clientId: '300122863410-r7lmnek6g2dosna1rfgtudqpoggik7ie.apps.googleusercontent.com',
  scope: 'profile email',
  prompt: 'select_account'
}
Vue.use(authGoogle, gauthOption)

Vue.use(VueSweetalert2)
Vue.use(VueYoutube)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
