import VueTable from "vuetable-2";
import Paginate from "vuejs-paginate";
import { apiCustomerGet } from "../../../services/baseApi.js";

const cssTable = {
  table: {
    tableWrapper: "table-responsive",
    tableHeaderClass: "",
    tableBodyClass: "",
    tableClass: "table table-hover table-striped table-borderless",
    loadingClass: "",
    ascendingIcon: "",
    descendingIcon: "",
    ascendingClass: "",
    descendingClass: "",
    sortableIcon: "",
    handleIcon: ""
  }
};

export default {
  components: {
    VueTable,
    Paginate
  },
  data() {
    return {
      fieldTable: ["address", "type"],
      cssTable,
      totalPage: 1,
      page: 1,
      totalItem: [],
      itemCurrentPage: [],
      perPage: 5
    };
  },
  methods: {
    // Call service get list wallet saved
    async getListWallet() {
      try {
        let res = await apiCustomerGet("customerWallet");

        if (res && res.data && res.data.data && res.data.data.customerWallets) {
          let totalItem = res.data.data.customerWallets;

          totalItem.map(item => {
            item.type = item.type === "BTC" ? "BTC" : "BSBT";
            return item;
          })
          this.totalItem = totalItem;

          this.totalPage = Math.ceil(this.totalItem.length / this.perPage);
          this.getItemCurrentPage();
        }
      } catch (error) {
        console.log("error");
      }
    },

    // Handle pagination
    getItemCurrentPage() {
      let indStart = (this.page - 1) * this.perPage;
      let indEnd = this.page * this.perPage;

      this.itemCurrentPage = this.totalItem.slice(indStart, indEnd);
    },
    paginate(page) {
      this.page = page;
      this.getItemCurrentPage();
    },
    prevPage() {
      if (this.page == 1) return;
      this.page -= 1;
      this.getItemCurrentPage();
    },

    // Choose a wallet
    chooseWallet(e) {
      setTimeout(() => {
        this.$emit("choose-wallet", e.data);

        let closeBtn = document.querySelector("#list-wallet-modal .close-btn");
        closeBtn.click();
      }, 500);
    }
  },
  mounted() {
    const myModal = document.getElementById("list-wallet-modal");

    myModal.addEventListener("show.bs.modal", () => {
      this.getListWallet();
    });

    this.getListWallet();
  }
};
