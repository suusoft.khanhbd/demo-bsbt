import sweetAlert from "../../helper/sweetAlert.js";
import InfoWallet from "./info-wallet/InfoWallet.vue";
import ListWallet from "./list-wallet/ListWallet.vue";
import { apiCustomerGet, apiCustomerPost } from "../../services/baseApi.js";

export default {
  components: {
    InfoWallet,
    ListWallet
  },
  data() {
    return {
      amountMoney: null,
      minAmount: 0.0001,
      typeMoney: "BTC",
      remainMoney: null,
      warningMsg: "",
      receiveAccount: "",
      walletDetail: {},
      walletDetailByConfirm: {},
      address: "",
      isVerifyWallet: false,
      user: {}
    };
  },
  watch: {
    amountMoney(val) {
      if (val && val.includes("-")) {
        this.amountMoney = val.split("-").join("");
      }
      if (Number(val) && Number(val) < this.minAmount) {
        this.remainMoney = null;
        this.warningMsg = `The minimum amount is ${this.minAmount}`;
        return;
      }
      if (
        (this.typeMoney === "BTC" && Number(val) > this.computeBtcBalance()) ||
        (this.typeMoney === "ETH" && Number(val) > this.computeBsbtBalance())
      ) {
        this.remainMoney = null;
        this.warningMsg = "The amount is greater than the account balance";
        return;
      } else {
        this.warningMsg = "";
      }

      if (this.typeMoney === "BTC") {
        let remainMoney = this.computeBtcBalance() - Number(val);
        return remainMoney + " BTC";
      }
      if (this.typeMoney === "ETH") {
        let remainMoney = this.computeBsbtBalance() - Number(val);

        return remainMoney + " BSBT";
      }
    },
    typeMoney() {
      this.amountMoney = null,
      this.warningMsg = ""
    }
  },
  methods: {
    computeBsbtBalance() {
      return this.user ? parseFloat(this.user.bsbtBalance).toFixed(8) : 0;
    },
    computeBtcBalance() {
      return this.user ? parseFloat(this.user.balanceAvailable).toFixed(8) : 0;
    },

    keypressWalletAddress() {
      this.isVerifyWallet = false;
      this.walletDetail = {};
    },

    // Get user instead
    // async getProfile() {
    //   try {
    //     const token = localStorage.getItem("token");
    //     if (!token) return;

    //     const get = await apiCustomerGet("auth/profile", {}, { token });
    //     this.user = get.data.data
    //   } catch (error) {
    //     console.log(error);
    //   }
    // },
    // Verify account is Mystarwallet
    async verifyAccount() {
      try {
        if (!this.receiveAccount) {
          sweetAlert.toastAlert(this, "error", {
            text: "Enter your wallet address"
          });
          return;
        }

        let body = {
          customerWallet: {
            type: this.typeMoney,
            address: this.receiveAccount
          }
        };

        let res = await apiCustomerPost("customerWallet", body);

        if (res && res.data && res.data.status && res.data.data) {
          this.walletDetail = res.data.data.customerWallet || res.data.data;
          this.isVerifyWallet = true;
          let result = await apiCustomerGet("customerWallet");
          result.data.data.customerWallets.filter((item) => {
            if (item.address == this.receiveAccount) {
              this.walletDetail = item;
            }
          });
        } else {
          sweetAlert.toastAlert(this, "error", {
            text: `To withdraw ${this.typeMoney === "BTC" ? "BTC" : "BSBT"}, please enter your wallet address ${
              this.typeMoney === "BTC" ? "BTC" : "BSBT"
            } Mystarwallet`
          });
        }
      } catch (error) {
        if (error && error.response && error.response.data.status === false) {
          sweetAlert.toastAlert(this, "error", {
            text: `To withdraw ${this.typeMoney === "BTC" ? "BTC" : "BSBT"}, please enter your wallet address ${
              this.typeMoney === "BTC" ? "BTC" : "BSBT"
            } Mystarwallet`
          });
        }
        this.isVerifyWallet = false;
        this.walletDetail = {};
      }
    },

    // List Wallet saved
    chooseWallet(data) {
      if (!data) return;

      this.walletDetail = data;
      console.log(this.walletDetail.id);
      this.receiveAccount = data.address;
      this.isVerifyWallet = true;

      if (data.type === "ETH" || data.type === "BSBT") {
        this.typeMoney = "ETH";
      } else if (data.type === "BTC") {
        this.typeMoney = "BTC";
      }
    },

    // Withdraw submit
    async submitWithdraw() {
      try {
        // Check info require
        if (!this.amountMoney || this.amountMoney == "0") {
          sweetAlert.toastAlert(this, "warning", "Enter the amount you want to withdraw");
          return;
        }
        if (!this.receiveAccount) {
          sweetAlert.toastAlert(this, "warning", "Enter your wallet address");
          return;
        }

        if (!this.walletDetail.id) {
          this.isVerifyWallet = false;
        }

        if (!this.isVerifyWallet) {
          sweetAlert.toastAlert(this, "warning", "Wallet not verified");
          return;
        }

        await sweetAlert.toastAlert(this, "info", {
          title: "Notice of withdrawal",
          text: `Withdrawal requests registered by 4:00 pm will be processed on the same day, only requests after 4:00 pm will be processed the next day. Withdrawal requests registered on Saturday, Sunday will be processed once on the following Monday. Thank you.`
        });

        let body = {
          amount: this.amountMoney,
          customer_wallet_id: this.walletDetail.id
        };
        let res = await apiCustomerPost("withdraw", body);
        if (res && res.data && res.data.data && res.data.status) {
          sweetAlert.toastAlert(this, "success", "Your request has been sent!", undefined, undefined, "Done", true, "Withdraw history").then(data => {
            console.log('data', data)
            if (data.isDismissed) {
              this.$router.push(`/withdraw-history/?type=${this.typeMoney === "BTC" ? "BTC" : "BSBT"}`);
            }
          });

          this.refreshData();
          // this.getUser();
          // this.getProfile()
        }
      } catch (error) {
        console.log(error.response);
        if (error && error.response && error.response.data && error.response.data.message) {
          sweetAlert.toastAlert(this, "error", error.response.data.message);
          return;
        }
        sweetAlert.toastAlert(this, "error", "There was an error");
      }
    },

    // Refresh data after submit form
    refreshData() {
      this.getUser();

      (this.amountMoney = null), (this.remainMoney = null);
      this.warningMsg = "";
      this.isVerifyWallet = false;
      this.receiveAccount = "";
      this.walletDetail = {};
    },

    // Update user info
    async getUser() {
      try {
        let token = localStorage.getItem("token");

        if (!token) {
          return;
        }

        let res = await apiCustomerGet("auth/profile", {}, { token });
        if (res && res.data && res.data.status && res.data.data) {
          res.data.data.isLogged = true;

          //Save data
          localStorage.setItem("user", JSON.stringify(res.data.data));
          this.$store.commit("setUser", res.data.data);
          this.user = res.data.data;
        }
      } catch (error) {
        console.log(error);
      }
    }
  },
  mounted() {
    this.getUser();
    let user = localStorage.getItem("user");
    if (!user) {
      this.$router.push("/signin").catch(() => {});
    }
    this.user = JSON.parse(user);
  }
};
