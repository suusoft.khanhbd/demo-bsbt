export default {
  props: {
    walletDetail: Object
  },
  computed: {
    walletName() {
      if (this.walletDetail && this.walletDetail.customer && this.walletDetail.customer.name) {
        return this.walletDetail.customer.name;
      }
      return "";
    }
  }
};
