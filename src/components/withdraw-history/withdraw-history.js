import VueTable from "vuetable-2";
import Paginate from "vuejs-paginate";
import moment from "moment";
import { apiCustomerGet } from "../../services/baseApi";

const cssTable = {
  table: {
    tableWrapper: "table-responsive",
    tableHeaderClass: "",
    tableBodyClass: "",
    tableClass: "table table-hover table-striped table-borderless",
    loadingClass: "",
    ascendingIcon: "",
    descendingIcon: "",
    ascendingClass: "",
    descendingClass: "",
    sortableIcon: "",
    handleIcon: ""
  }
};

export default {
  components: {
    VueTable,
    Paginate
  },
  data() {
    return {
      fieldTable: ["address", "date", "coin", "amount", { name: "remain-amount", title: "Remain Amount" }, "status"],
      user: {},
      cssTable,
      totalPage: 1,
      page: 1,
      totalItem: [],
      itemCurrentPage: [],
      perPage: 10,
      typeTable: "BTC"
    };
  },
  methods: {
    async getListItem() {
      try {
        let res = await apiCustomerGet("withdraw");
        if (res && res.data && res.data && res.data.status) {
          let listData = res.data.data.withdraws;

          this.getTotalItem(listData);
          this.getItemCurrentPage();
        }
      } catch (error) {
        console.log(error);
      }
    },
    getTotalItem(listData) {
      listData = listData
        .map((item) => {
          let { amount, createdAt: date, status } = item;

          date = date && moment.unix(date).format("DD/MM/YYYY");

          let addressWallet = item.customerWallet.address;
          let typeWallet = item.customerWallet.type;
          let remainAmout = this.typeTable === "BTC" ? item.remain_amount_btc : item.remain_amount_bsbt;
          return {
            amount,
            date,
            status,
            "remain-amount": remainAmout,
            coin: typeWallet === "ETH" ? "BSBT" : "BTC",
            address: addressWallet
          };
        })
        .filter((item) => {
          if (item.coin === "ETH" && this.typeTable === "BSBT") return true;
          return item.coin === this.typeTable;
        });

      this.totalPage = Math.ceil(listData.length / this.perPage);
      this.totalItem = listData;
    },
    getItemCurrentPage() {
      let indStart = (this.page - 1) * this.perPage;
      let indEnd = this.page * this.perPage;

      this.itemCurrentPage = this.totalItem.slice(indStart, indEnd);
    },
    paginate(page) {
      this.page = page;
      this.getItemCurrentPage();
    },
    prevPage() {
      if (this.page == 1) return;
      this.page -= 1;
      this.getItemCurrentPage();
    }
  },
  mounted() {
    const user = localStorage.getItem("user") || "{}";
    this.user = JSON.parse(user);

    let typeTable = this.$route.query.type || "BTC";
    this.typeTable = typeTable;

    this.getListItem(typeTable);
  }
};
