import Vue from "vue";
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    user: {},
    error: {}
}

const mutations = {
    setUser(state, data) {
        state.user = data;
    },
    setError(state, data) {
        state.error = data;
    }
}

const store = {
    state,
    mutations
}

export default new Vuex.Store(store);