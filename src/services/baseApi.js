import axios from "axios";

const base = "https://api.bsbtoken.com/api/";
//const base = "http://192.168.0.131:8000/api/";
// const base = "http://localhost:8000/api/";
// const base = process.env.BASE_API

const apiCustomerGet = (url, body, header = {}) => {
  let token = localStorage.getItem("token");
  if (token) {
    header.token = token;
  }

  return axios({
    url: `${base}customer/${url}`,
    method: "get",
    headers: header,
    params: body
  });
};

const apiCustomerPost = (url, body, header = {}) => {
  let token = localStorage.getItem("token");
  if (token) {
    header.token = token;
  }

  return axios({
    url: `${base}customer/${url}`,
    method: "post",
    data: body,
    headers: header
  });
};

export { apiCustomerGet, apiCustomerPost };
