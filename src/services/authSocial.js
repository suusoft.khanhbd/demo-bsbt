import { apiCustomerPost } from "./baseApi.js";

const fbAuth = (thisVue) => {
  if (!window.FB) return;

  let fbToken = null;

  async function callLoginService() {
    try {
      let res = await apiCustomerPost("auth/login/fb", {
        fbToken
      });

      if (res && res.data && res.data.status && res.data.data.token) {
        localStorage.setItem("token", res.data.data.token);
        thisVue.$router.push('/').catch(() => {});
      }
    } catch (error) {
      console.log(error);
    }
  }

  // Login dialog
  window.FB.login(function(response) {
    if (response.authResponse) {
      if (response.authResponse.accessToken) {
        fbToken = response.authResponse.accessToken;
        callLoginService();
      }
      console.log("Welcome!  Fetching your information.... ");
      // window.FB.api("/me?fields=name,email", function(response) {
      //   console.log("Good to see you, " + response.name + ".");
      // });
    } else {
      console.log("User cancelled login or did not fully authorize.");
    }
  });
};

export { fbAuth };
