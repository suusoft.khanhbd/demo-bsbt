const allStatus = {
  0: "Pending",
  1: "Approved",
  2: "Failed"
};

// Get status
const getStatus = (code) => allStatus[code];

// Get all status
const getAllStatus = () => allStatus;

export { getStatus, getAllStatus };
