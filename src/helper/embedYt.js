const getHtml = (url, autoplay = 0) => {
  if (!url) return "";
  if (!url.includes("embed")) {
    let id = url.includes("v=") ? url.split("v=")[1].split("&")[0] : url.split("/").pop();

    url = `https://www.youtube.com/embed/${id}`;
  }
  url += `?autoplay=${autoplay}&controls=0`;

  return `<iframe width="100%" height="100%" src="${url}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
};

const getThumb = (url) => {
  if (!url) return "";

  let id = url.includes("v=") ? url.split("v=")[1].split("&")[0] : url.split("/").pop();
  let result = `https://img.youtube.com/vi/${id}/sddefault.jpg`;
  return result;
};

const getIdVid = (url) => {
  if (!url) return "";

  let id = url.includes("v=") ? url.split("v=")[1].split("&")[0] : url.split("/").pop();
  return id;
};

export default {
  getHtml,
  getThumb,
  getIdVid
};
