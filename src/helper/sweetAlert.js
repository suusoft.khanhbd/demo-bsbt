const toastAlert = (
  thisVue,
  icon,
  title,
  timer = false,
  showConfirmButton = true,
  confirmButtonText = "Ok",
  showCancelButton = false,
  cancelButtonText = "Cancel",
  position = "center",
  timerProgressBar = false
) => {
  return (thisVue.$swal.fire({
    toast: false,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", thisVue.$swal.stopTimer);
      toast.addEventListener("mouseleave", thisVue.$swal.resumeTimer);
    },
    icon,
    title: title && title.title ? title.title : typeof title === "object" ? undefined : title,
    text: title && title.text ? title.text : undefined,
    position,
    timer,
    showConfirmButton,
    showCancelButton,
    confirmButtonText,
    cancelButtonText,
    timerProgressBar,
    customClass: {
      confirmButton: "px-5 rounded-pill",
      cancelButton: "px-5 rounded-pill"
    }
  }));
};

export default { toastAlert };
