const allStatus = {
  0: "Pending",
  1: "Approved",
  2: "Failed"
};

// Get status
const getStatus = (code) => {
  if (code === undefined) return 'Pending';

  return allStatus[code] ? allStatus[code] : "Pending";
};

// Get all status
const getAllStatus = () => allStatus;

export default { getStatus, getAllStatus };
